package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {
	
	@FindBy(how=How.ID, using="twotabsearchtextbox") 
	WebElement searchInput;
	
	public HomePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	
	public void writeOnSearchInput(String text) {
		searchInput.sendKeys(text);
	}	
	
	public void pressEnterOnSearchInput() {
		searchInput.sendKeys(Keys.ENTER);
	}
	
	

}
