package pages;

import java.util.Comparator;

public class Product {
	
	String name;
	Float rating;
	Float price;
	
	public Product() {
		
	}
	
	public Product(String name, Float rating, Float price) {
		this.name = name;
		this.rating = rating;
		this.price = price;
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		name = name;
	}
	public Float getRating() {
		return rating;
	}
	public void setRating(Float rating) {
		rating = rating;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		price = price;
	}
	
	public static Comparator<Product> producPriceComparator = new Comparator<Product>() {
		public int compare(Product s1, Product s2) {
			Float productPrice1 = s1.getPrice();
			Float productPrice2 = s2.getPrice();

			// ascending order
			return productPrice1.compareTo(productPrice2);

		}
	};
	
	public static Comparator<Product> producRatingComparator = new Comparator<Product>() {
		public int compare(Product s1, Product s2) {
			Float productPrice1 = s1.getRating();
			Float productPrice2 = s2.getRating();

			// ascending order
			return productPrice1.compareTo(productPrice2);

		}
	};
	
	public static boolean ComparePrices(Product s1, Product s2) {
		if (s1.getPrice() == s2.getPrice() && s1.getName() == s2.getName()) {
			System.out.println(s1.getPrice() + "==" + s2.getPrice());
			return true;
		}
		return false;
	}
	
	

}
