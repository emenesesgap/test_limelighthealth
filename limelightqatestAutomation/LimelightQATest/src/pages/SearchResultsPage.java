package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SearchResultsPage extends BasePage{
	
	@FindBy(how=How.NAME, using="s-ref-checkbox-8080061011") 
	WebElement plasticCaseOption;
	
	@FindBy(how=How.ID, using="high-price") 
	WebElement maxPriceInput;
	
	@FindBy(how=How.ID, using="low-price") 
	WebElement minPriceInput;
	
	@FindBy(how=How.XPATH, using="//span[@class=\"a-button a-spacing-top-mini s-small-margin-left\" and contains(., \"Go\")]") 
	WebElement goButton;
	
	@FindBy(how=How.XPATH, using="//span[@data-hook=\"rating-out-of-text\"]") 
	WebElement ratingElementLi;
	
	ArrayList<Product> listProduct;

	public ArrayList<Product> getListProduct() {
		return listProduct;
	}

	public void setListProduct(ArrayList<Product> listProduct) {
		this.listProduct = listProduct;
	}

	public SearchResultsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	public void selectPlasticOption() {
		plasticCaseOption.click();
	}
	
	public void setMinPrice(String value) {
		waitElement(minPriceInput);
		minPriceInput.sendKeys(value);
	}
	
	public void setMaxPrice(String value) {
		waitElement(maxPriceInput);
		maxPriceInput.sendKeys(value);
	}
	
	public void clickOnGoPricesButton() {
		goButton.click();
	}
	
	// this function get first five elements and add the items into arraylist
	public ArrayList<Product> getListItemsAmazon() {
		WebElement listElements = driver.findElement(By.id("s-results-list-atf"));
		List<WebElement> names = listElements.findElements(By.tagName("h2")); 
		List<WebElement> prices = listElements.findElements(By.className("sx-price-whole"));
		List<WebElement> ratings = listElements.findElements(By.xpath("//span[@class=\"a-declarative\"]//i[contains(@class,\"a-icon a-icon-sta\")]"));
		listProduct = new ArrayList<>();
		
		for (int i = 0; i < 5; i++)
		{
			Float price = Float.parseFloat(prices.get(i).getText());
			String rating = ratings.get(i).getAttribute("textContent");
			Float score = Float.parseFloat(rating.replace(" out of 5 stars", ""));
			Product product = new Product(names.get(i).getText(), score, price);   
		    listProduct.add(product);
		}
		
		return listProduct;
					
	}
	
	// just print the fives elements
	public void printListItemsAmazon(ArrayList<Product> listProducts) {
		for (Product product:listProducts) {
			System.out.println(String.format("Item name: %s ", product.getName()));
			System.out.println(String.format("Item rating: %s ", product.getRating()));
			System.out.println(String.format("Item price: %s ", product.getPrice()));		
			System.out.println("---------------------------------------------");
		}
	}
	
	// this function verify the prices
	public boolean verifyPrices(Product product) {
		boolean flag = false;
		if (product.getPrice() >= 20 && product.getPrice() <= 100) {
			flag = true;
		}
		return flag;
	}

}
