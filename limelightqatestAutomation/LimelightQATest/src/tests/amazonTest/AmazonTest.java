package tests.amazonTest;

import java.util.ArrayList;
import java.util.Collections;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import pages.HomePage;
import pages.Product;
import pages.SearchResultsPage;
import selenium.DriverSetup;

public class AmazonTest {
	
	private WebDriver driver;
	SoftAssert softas; 
	ArrayList<Product> itemsAmazon = new ArrayList<>();
	
	@BeforeClass(alwaysRun = true)
	public void setupClass()
	{
		driver = DriverSetup.setupDriver(DriverSetup.Browser.Chrome, "chromedriver.exe");
		driver.get("https://www.amazon.com");
		driver.manage().window().maximize();
	}
	
	@BeforeMethod(alwaysRun = true)
	public void setupTest()
	{
	}

	@Parameters()
	@Test(description = "Test to find products ")
	public void testProducts() throws Exception{
		
		HomePage homePage = new HomePage(driver);
		homePage.writeOnSearchInput("ipad air 2 case");
		homePage.pressEnterOnSearchInput();
		SearchResultsPage searchResultPage = new SearchResultsPage(driver);
		searchResultPage.selectPlasticOption();
		searchResultPage.setMinPrice("20");
		searchResultPage.setMaxPrice("100");
		searchResultPage.clickOnGoPricesButton();
		itemsAmazon = searchResultPage.getListItemsAmazon();
		searchResultPage.printListItemsAmazon(itemsAmazon);
		softas = new SoftAssert();
		for(Product product:itemsAmazon) {
			softas.assertEquals(searchResultPage.verifyPrices(product), true, "The following item, does not have the price requirements: " +product.getName());
		}
		
		softas.assertAll();
		
	}
	
	@Test(description = "Sort Test")
	public void testSortItems() {
		
		ArrayList<Product> listItems = itemsAmazon;
		System.out.println("---------------------------------------------");
		System.out.println("Products sort by Price");
		Collections.sort(listItems, Product.producPriceComparator);
		for (Product product:listItems) {
			System.out.println("Item name: " + product.getName());
			System.out.println("Item rating: " + product.getRating());
			System.out.println("Item price: " + product.getPrice());
			System.out.println("---------------------------------------------");
			System.out.println("---------------------------------------------");
		}
		
		System.out.println("---------------------------------------------");
		System.out.println("Products sort by Rating");
		Collections.sort(listItems, Product.producRatingComparator);
		for (Product product:listItems) {
			System.out.println("Item name: " + product.getName());
			System.out.println("Item rating: " + product.getRating());
			System.out.println("Item price: " + product.getPrice());
			System.out.println("---------------------------------------------");
			System.out.println("---------------------------------------------");
		}
		
		Collections.sort(listItems, Product.producPriceComparator);
		softas = new SoftAssert();
		for (int i=0; i< itemsAmazon.size(); i++) {
			softas.assertEquals(true, Product.ComparePrices(itemsAmazon.get(i), listItems.get(i)),"The items not matches");
			
		}
		softas.assertAll();
	}
	
	@Test(description = "Recomendations")
	public void testRecomendation() {
		Product byPrice;
		Product byRating;
		Collections.sort(itemsAmazon, Product.producRatingComparator);
		byRating = itemsAmazon.get(itemsAmazon.size()-1);
		Collections.sort(itemsAmazon, Product.producPriceComparator);
		
		int best = 0;
		for(int i =0; i < itemsAmazon.size(); i++) {
			
			Float price = itemsAmazon.get(i).getPrice();
			if (price < itemsAmazon.get(best).getPrice()) {
				best = i;
			}
		}
		
		byPrice = itemsAmazon.get(best);
		
		System.out.println("The case to buy  for rating is: " + byRating.getName() + " Rating= " + byRating.getRating());
		System.out.println("The case to buy  for price is: " + byPrice.getName() + " Price= " + byPrice.getPrice());
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDownTest()
	{
		driver.quit();
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDownClass()
	{

	}

}
